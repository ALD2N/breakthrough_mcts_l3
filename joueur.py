import interface
import numpy as np
import random as rd
import time

class Joueur:
    def __init__(self, partie, couleur, opts={}):
        self.couleur = couleur
        self.couleurval = interface.couleur_to_couleurval(couleur)
        self.jeu = partie
        self.opts = opts

    def demande_coup(self):
        pass

class Node:
	def __init__(self, couleur, coup, parent=None):
		self.couleur = couleur
		self.coup = coup
		self.parent = parent
		self.children = []
		self.nbVictoire

class Humain(Joueur):

    def demande_coup(self):
        pass


class IA(Joueur):

    def __init__(self, partie, couleur, opts={}):
        super().__init__(partie, couleur, opts)
        self.temps_exe = 0
        self.nb_appels_jouer = 0


class Random(IA):

    def demande_coup(self):
        listeCoups = self.jeu.plateau.liste_coups_valides(self.couleurval)
        nombreCoups = len(listeCoups)
        nb = rd.randint(0, nombreCoups-1)
        case = listeCoups[nb]
        return case


class Minmax(IA):

	def minmax(self,plateau,joueur,profondeur=3):

			if profondeur <= 0:
				return (plateau.evaluation(self.couleurval),None)

			if joueur == self.couleurval:
				m = -np.infty
				tabCoups = plateau.liste_coups_valides(joueur)

				for i in range(len(tabCoups)):
					p = plateau.copie()
					p.jouer(tabCoups[i],joueur)
					self.nb_appels_jouer += 1
					res,coup = self.minmax(p,-joueur,profondeur-1)

					if m < res:
						m = res
						argmax = tabCoups[i]

				return(m,argmax)

			else:
				m = np.infty
				tabCoups = plateau.liste_coups_valides(-joueur)

				for i in range(len(tabCoups)):
					p = plateau.copie()
					p.jouer(tabCoups[i],-joueur)
					self.nb_appels_jouer += 1
					res, coup = self.minmax(p,joueur,profondeur-1)
					if m > res:
						m = res
						argmin = tabCoups[i]
				return(m,argmin)


	def demande_coup(self):
		temps_debut = time.time()
		eval, coup = self.minmax(self.jeu.plateau,self.couleurval)
		temps_fin = time.time()

		self.temps_exe = self.temps_exe + (temps_fin-temps_debut)
		print(round(self.temps_exe,3))

		return coup


class AlphaBeta(IA):

	def alphabeta(self, plateau, joueur, profondeur=3, alpha=-np.infty, beta=np.infty):
		if profondeur<=0:
			return (plateau.evaluation(self.couleurval), None)
		if joueur==self.couleurval: #recherche du meilleur coup
			M=-np.infty
			tabCoups=plateau.liste_coups_valides(joueur)
			for i in range(len(tabCoups)):
				p=plateau.copie()
				p.jouer(tabCoups[i], joueur)
				val, coup=self.alphabeta(p, -joueur, profondeur-1, alpha, beta)
				if M<val:
					M=val
					argmax=tabCoups[i]
					alpha=max(alpha, M)
				if alpha>=beta:
					break
			return (M, argmax)
		else:
			m=np.infty
			tabCoups=plateau.liste_coups_valides(-joueur)
			for i in range(len(tabCoups)):
				p=plateau.copie()
				p.jouer(tabCoups[i], -joueur)
				val, coup=self.alphabeta(p, joueur, profondeur-1, alpha, beta)
				if m>val:
					m=val
					argmin=tabCoups[i]
					beta=min(beta, m)
				if alpha>=beta:
					break
			return (m, argmin)


	def demande_coup(self):
		tempsDebut=time.time()
		valeur, case=self.alphabeta(self.jeu.plateau, self.couleurval)
		tempsFin=time.time()

		self.temps_exe+=(tempsFin-tempsDebut)
		self.nb_appels_jouer+=1

		print(round(self.temps_exe,3), "secondes d'exécution")
		print(self.nb_appels_jouer,"nombre de fois qu'il joue")
		return case

class MonteCarlo(IA):
	def monte_carlot(self,plateau,couleur):
		tabCoup = plateau.liste_coups_valides(couleur)
		tabVictoire = []
		nbSimu = 1000

		for coup in tabCoup:
			victoire = 0
			p = plateau.copie()
			p.jouer(coup,couleur)

			for i in range(nbSimu):
				joueurCourant = -couleur

				while(not p.check_partie_finie()):
					p2 = p.copie()
					listeCoup2 = p2.liste_coups_valides(joueurCourant)
					p2.jouer(listeCoup2[np.random.randint(0,len(listeCoup2))],joueurCourant)
					joueurCourant = -joueurCourant

				if p2.check_partie_finie():
					if p2.vainqueur() == couleur:
						victoire += 1
			tabVictoire.append(victoire)

		return tabCoup[tabVictoire.index(max(tabVictoire))]

	def demande_coup(self):
		coup=self.monte_carlot(self.jeu.plateau,self.couleurval)
		return coup

#Monte-Carlos Search Tree Vanilla
class MCTSV(IA):
	def selection(node):
		coup = node.children[np.random.randint(0,len(node.children))]
