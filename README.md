# Projet pour le module d'intelligence artificielle pour les étudiants de L3

Le but de ce projet est d'apprendre à explorer l'arbre de jeu via MCTS.

## Rappel

Pour rappel voici un lien vers les commandes git utiles pour ce module :
https://juliendehos.gitlab.io/posts/env/post20-git.html#forker-un-d%C3%A9p%C3%B4t-existant

## Etape 1 : Installation

- Forker ce dépôt.

Les paquets nécessaires sont `numpy`, `pillow` et `tkinter`.

## Etape 1 : Création d'un bot aléatoire

## Etape 2 : Création d'un bot Minmax ou Alpha-Beta

Pour cela, commencez par créer une heuristique. Pour pourrez associer des points à chaque pion en fonction de la distance qui le sépare de l'arrivée.

## Etape 3 : Création d'un bot MCTS vanilla

## Etape 4 : Création d'un bot UCT

EXAMEN :

1) Les noeuds correspondent à des choix, les arêtes à leurs probabilitées et les feuilles à un état final du jeu.

2) Il est plus intéressant de visiter les noeuds avec un winrate/visites plutôt bas, cela permet de prioriser la recherche de solution et d'ainsi ne pas explorer toute les possibilités.

3) Cela permet de le rendre plus efficace en lui donnant une évaluation de ses actions.

4) On peut donner une évaluation par rapport au placement de la pièce, sa valeur estimé selon le moment de la partie (par exemple un cavalier a plus de valeur au début de la partie aux échecs).

5)Je pense qu'il est plus intéressant d'utiliser MinMax sur des arbres avec une taille plutôt réduite alors que MCTS est plutôt indiqué pour des arbres de taille plus conséquentes.

6) Il donne une Victoire

7)

8) Il s'agit d'un jeu à information complètes


9) C'est une situation où les 2 joueurs rationnels arrivent à prévoir avec succès les actions de leur adversaire et de maximiser leur victoire avec ces informations

10.a)

   | A | B |
------------
  M| 3 | 1 |
------------
 DC| 2 | 4 |
------------
BHL| 0 | 0 |

10.b) Non car un humain ne peut pas réellement être considéré comme rationnel sauf si c'est précisé (mais c'est un peu irréaliste).



